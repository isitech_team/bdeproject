package com.main.bdeproject.controller;

import org.restlet.Restlet;
import org.restlet.resource.ServerResource;
//import org.restlet.routing.Router;
import org.restlet.routing.Router;

import com.main.bdeproject.service.ServiceHelloWorld;

public class Controller extends ServerResource {

   /* @Get
    public String handleGetGroupe() {
       *//* String query = getQuery().getQueryString();
        System.out.print(query);
        String name = getQueryValue("name");*//*

        *//*ServiceGroupe groupServ = ServiceGroupe();
        List<Groupe> lst = groupServ.GetGroupe();*//*

        String nameRequest = getRequest().getRootRef().toString();

        List<Groupe> lst = new ArrayList<Groupe>();
        lst.add(new Groupe("Groupe ISITECH"));
        lst.add(new Groupe("Groupe TITI"));
        lst.add(new Groupe("Groupe TOTO"));

        Gson obj = new Gson();

        return obj.toJson(lst);
    }

*//*
    @Get("/eleve")
    public String handleGetEleve() {
       *//**//* String query = getQuery().getQueryString();
        System.out.print(query);
        String name = getQueryValue("name");*//**//*
        return "{HTTP GET, eleve}";
    }*//*

    @Post
    public String handlePost() {
        return "HTTP POST.";
    }

    @Put
    public String handlePut() {
        return "HTTP PUT.";
    }

    @Delete
    public String handleDelete() {
        return "HTTP DELETE.";
    }
*/
/*    public Restlet createRoot() {
        Router router = new Router(getContext());
        router.attach("/group/{word}", ServiceGroupe.class);
        router.attach("/dictionary/{word}", dictionaryRestlet);
        return router;
    }*/

    public synchronized Restlet createInboundRoot() {
        Router router = new Router(getContext());
        router.attach("/helloWorld", ServiceHelloWorld.class );
        return router;
    }
}
