package com.main.bdeproject.service;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import com.main.bdeproject.bean.Eleve;
import com.main.bdeproject.bean.Evenement;
import com.main.bdeproject.dao.ConnexionBdd;
import com.owlike.genson.Genson;
import org.restlet.resource.Get;
import org.restlet.resource.Post;
import org.restlet.resource.ServerResource;

public class ServiceEvenement extends ServerResource {

	private ConnexionBdd	connexion;
	public ServiceEvenement()
	{
		this.connexion = ConnexionBdd.getInstance();
	}

	// @post
	public Evenement creerEvenement(String type, int duree, String date,
			String libelle)
	{
		Evenement event = new Evenement(type, duree, date, libelle);
		connexion
				.update("insert into EVENEMENT (type_element,duree,date,libelle) values('"
						+ event.getType() + "'," + event.getDuree()
						+ ",'" + event.getDate() + "','" + event.getLibelle() + "')");
		return event;
	}

	// @put
	public Evenement majEvenement(int id, String type, int duree,
			String date, String libelle)
	{
		connexion.update("update EVENEMENT set type_element='" + type
				+ "', libelle='" + libelle + "',duree=" + duree + ", date='"
				+ date + "' WHERE id_evenement=" + id);
		ResultSet rs = connexion
				.query("SELECT * FROM evenement where id_evenement=" + id);
		Evenement event = new Evenement();
		int idEvent;
		String typeEvent;
		int dureeEvent;
		String dateEvent;
		String libelleEvent;
		try
		{
			while (rs.next())
			{
				idEvent = rs.getInt("id_evenement");
				typeEvent = rs.getString("type_element");
				dureeEvent = rs.getInt("duree");
				dateEvent = rs.getString("date");
				libelleEvent = rs.getString("libelle");
				event.setIdEvenement(idEvent);
				event.setDate(dateEvent);
				event.setDuree(dureeEvent);
				event.setLibelle(libelleEvent);
			}
		}
		catch (SQLException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return event;
	}

    @Post
    public String poserEvenement(String event)
    {
        Genson genson = new Genson.Builder().setWithClassMetadata(true).create();
        Evenement eventObj;
        String json = null;

        try{
            eventObj = genson.deserialize(event, Evenement.class);


            eventObj = creerEvenement(eventObj.getType(), eventObj.getDuree(), eventObj.getDate(),eventObj.getLibelle());
            //lst = genson.deserialize(json, new GenericType<List<Eleve>>(){});
            json = genson.serialize(eventObj);
        }
        catch(Exception ex){
            System.out.println(ex.getMessage());
        }

        return  json;
    }

	// @get
    public List<Evenement> getEvenement() {
        return getEvenement(-1);
    }
	public List<Evenement> getEvenement(int id) {
		List<Evenement> liste = new ArrayList<Evenement>();
		try
		{
            String myquery;
            if(id<0){
                myquery="select * from evenement";
            }else{
                myquery="select * from evenement where id_evenement="+id;
            }

			ResultSet rs = connexion.query(myquery);
			try
			{
				while (rs.next())
				{
					int idEvenement = rs.getInt("id_evenement");
					String type = rs.getString("type_element");
					int duree = rs.getInt("duree");
					String date = rs.getString("date");
					String libelle = rs.getString("libelle");
					liste.add(new Evenement(idEvenement,type, duree, date, libelle));
					// TODO: check structure bdd
				}
			}
			finally
			{
				rs.close();
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return liste;
	}

    /*
       http://192.168.0.1:9001/bdeproject/evenement
       http://192.168.0.1:9001/bdeproject/evenement?id=5
   */
    @Get
    public String getEleveWebService() {
        int id;

        try {
            id = Integer.valueOf(getQuery().getValues("id"));
        }
        catch (Exception ex) {
            id = -1;
        }

        List<Evenement> lst;

        if(id > 0) {
            lst = getEvenement(id);
        }else {
            lst = getEvenement();
        }

        Genson genson = new Genson.Builder().setWithClassMetadata(true).create();

        String json;
        try {
            json = genson.serialize(lst);
        }
        catch (Exception ex) {
            json = "{" + ex.getMessage() + "}";
        }

        return json;
    }
}