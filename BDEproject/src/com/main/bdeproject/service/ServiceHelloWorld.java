package com.main.bdeproject.service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.main.bdeproject.bean.Evenement;
import com.main.bdeproject.bean.Groupe;
import com.owlike.genson.Genson;
import org.restlet.resource.Get;
import org.restlet.resource.ServerResource;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Etudiant on 11/02/2015.
 */
public class ServiceHelloWorld extends ServerResource {

    @Get("groupe")
    public String handleGetGroupe() {
        List<Groupe> lst = new ArrayList<Groupe>();
        lst.add(new Groupe("Groupe ISITECH"));
        lst.add(new Groupe("Groupe TITI"));
        lst.add(new Groupe("Groupe TOTO"));

        Genson genson = new Genson.Builder().setWithClassMetadata(true).create();

        String json;
        try {
            json = genson.serialize(lst);
        }
        catch (Exception ex) {
            json = "{" + ex.getMessage() + "}";
        }

       // return obj.toJson(lst);
        return json;
    }

    @Get("event")
    public String handleGetEvent() {
        List<Evenement> lst = new ArrayList<Evenement>();
        lst.add(new Evenement("Evenement ISITECH",58,"20/06/2015","Journée Ski"));
        lst.add(new Evenement("Evenement TITI",199,"20/06/2015","Journée Todo"));
        lst.add(new Evenement("Evenement TOTO",1567,"20/06/2015","Journée feu"));

        Genson genson = new Genson.Builder().setWithClassMetadata(true).create();

        String json;
        try {
            json = genson.serialize(lst);
        }
        catch (Exception ex) {
            json = "{" + ex.getMessage() + "}";
        }

        // return obj.toJson(lst);
        return json;
    }
}
