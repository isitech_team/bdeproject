package com.main.bdeproject.service;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import com.google.gson.Gson;
import com.main.bdeproject.bean.Groupe;
import com.main.bdeproject.dao.ConnexionBdd;
import com.owlike.genson.Genson;
import org.restlet.resource.Get;
import org.restlet.resource.ServerResource;

public class ServiceGroupe extends ServerResource
{
	private ConnexionBdd	connexionBdd;
	public ServiceGroupe()
	{
		connexionBdd = ConnexionBdd.getInstance();
	}
	// post
	public Groupe creerGroupe(String nomGroupe)
	{
		Groupe groupe = new Groupe(nomGroupe);
		connexionBdd.update("INSERT INTO Groupe (nom_groupe) VALUES ('"
				+ groupe.getNomGroupe() + "')");
		Gson obj = new Gson();
		// return obj.toJson(groupe);
		return groupe;
	}
	// put
	public Groupe majGroupe(int id, String nomGroupe)
	{
		// on maj tout les champs ou surcharge methode en fonction du champs
		// modifié ?
		// quels champs modifiables ?
		connexionBdd.update("UPDATE Groupe SET nom_groupe = '" + nomGroupe
				+ "' WHERE id_groupe = " + id);
		ResultSet rs = connexionBdd
				.query("SELECT * FROM Groupe WHERE id_groupe = " + id);
		String nom;
		int idGroupe;
		Groupe groupe = null;
		try
		{
			while (rs.next())
			{
				nom = rs.getString("nom_groupe");
				idGroupe = rs.getInt("id_groupe");
				groupe = new Groupe();
				groupe.setIdGroupe(idGroupe);
				groupe.setNomGroupe(nom);
			}
		}
		catch (SQLException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return groupe;
	}
	// get
	public List<Groupe> getGroupe()
	{
		List<Groupe> listeGroupe = new ArrayList<Groupe>();
		ResultSet rs = connexionBdd.query("SELECT * FROM Groupe");
		String nom = "";
		int idGroupe = -1;
		
		try
		{
			while (rs.next())
			{
				Groupe groupe = new Groupe();
				nom = rs.getString("nom_groupe");
				idGroupe = rs.getInt("id_groupe");
				groupe.setIdGroupe(idGroupe);
				groupe.setNomGroupe(nom);
				listeGroupe.add(groupe);
			}
		}
		catch (SQLException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return listeGroupe;
	}

    @Get
    public String getGroupeWebService() {

        List<Groupe> lst = getGroupe();

        Genson genson = new Genson.Builder().setWithClassMetadata(true).create();

        String json;
        try {
            json = genson.serialize(lst);
            json = json + " parameter " + getQuery().getValues("id");
        }
        catch (Exception ex) {
            json = "{" + ex.getMessage() + "}";
        }

        return json;
    }
}
