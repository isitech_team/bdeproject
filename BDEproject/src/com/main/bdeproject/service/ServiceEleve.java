package com.main.bdeproject.service;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.main.bdeproject.bean.Eleve;
import com.main.bdeproject.dao.ConnexionBdd;
import com.owlike.genson.Genson;
import org.restlet.resource.Get;
import org.restlet.resource.Put;
import org.restlet.resource.ServerResource;

public class ServiceEleve extends ServerResource {

    private ConnexionBdd connexion;


    public ServiceEleve() {
        this.connexion = ConnexionBdd.getInstance();
    }

    // @post
    public Eleve creerEleve(String nom, String prenom, String nomClasse) {
        Eleve myEleve = new Eleve(nom, prenom, nomClasse);
        connexion.update("INSERT INTO Eleve (nom_eleve,prenom,nom_classe)  VALUES('" + myEleve.getNomEleve() + "','"+ myEleve.getPrenomEleve() + "','" + myEleve.getNomClasse()+ "')");
        return myEleve;
    }

    @Put
    public String majEleve(Eleve eleve) {
        Eleve obj = majEleve(eleve.getIdEleve(), eleve.getNomClasse(), eleve.getPrenomEleve(),eleve.getNomClasse());

        Genson genson = new Genson.Builder().setWithClassMetadata(true).create();

        String json;
        try {
            json = genson.serialize(obj);
        }
        catch (Exception ex) {
            json = "{" + ex.getMessage() + "}";
        }

        return json;
    }

    public Eleve majEleve(int id, String nomEleve, String prenomEleve,String nomClasse) {
        connexion.update("UPDATE Eleve SET nom_eleve='"+nomEleve+"', prenom='"+prenomEleve+"',nom_classe='"+nomClasse+"'WHERE id_eleve="+ id);
        ResultSet rs = connexion.query("SELECT * FROM Eleve where id_eleve="
                + id);
        Eleve myEleve=new Eleve();
        int idEle;
        String nom;
        String prenom;
        String classe;
        try {
            while (rs.next()) {

                idEle = rs.getInt(1);
                nom = rs.getString(2);
                prenom = rs.getString(3);
                classe = rs.getString(4);
                myEleve.setIdEleve(idEle);
                myEleve.setNomEleve(nom);
                myEleve.setPrenomEleve(prenom);
                myEleve.setNomClasse(classe);
            }
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return myEleve;
    }

    /*
        http://192.168.0.1:9001/bdeproject/eleve
        http://192.168.0.1:9001/bdeproject/eleve?id=5
    */
    @Get
    public String getEleveWebService() {
        int id;

        try {
            id = Integer.valueOf(getQuery().getValues("id"));
        }
        catch (Exception ex) {
            id = -1;
        }

        List<Eleve> lst;

        if(id > 0) {
            lst = getEleve(id);
        }else {
            lst = getEleve();
        }

        Genson genson = new Genson.Builder().setWithClassMetadata(true).create();

        String json;
        try {
            json = genson.serialize(lst);
        }
        catch (Exception ex) {
            json = "{" + ex.getMessage() + "}";
        }

        return json;
    }

    public List<Eleve> getEleve() {
        return getEleve(-1);
    }


    public List<Eleve> getEleve(int id) {
        String myquery;
        if(id<0){
            myquery="select * from Eleve";
        }else{
            myquery="select * from Eleve where id_eleve="+id;
        }
        ResultSet rs = connexion.query(myquery);
        List<Eleve> listEleve=new ArrayList<Eleve>();
        try {
            while (rs.next()) {
                int idEleve = Integer.valueOf(rs.getString(1));
                String nom = rs.getString(2);
                String prenom = rs.getString(3);
                String classe = rs.getString(4);
                Eleve myEleve = new Eleve(idEleve,nom, prenom, classe);
                listEleve.add(myEleve);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return listEleve;
    }
}