/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.main.bdeproject.service;

import com.main.bdeproject.bean.Evenement;
import com.main.bdeproject.view.ConsoleOutput;
import com.main.bdeproject.view.GridView;
import com.owlike.genson.Genson;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.core.util.MultivaluedMapImpl;
import java.util.List;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.UriBuilder;
import com.sun.jersey.api.client.ClientResponse;

/**
 *
 * @author Windows-Work
 */
public class ServiceEvenement {
     
   
    public List<Object> getLstEvenement(){

        List<Object> lst = getRequest();        
        for(Object obj : lst){
            ConsoleOutput.addOutPut(obj.toString());
        }
        
        return lst;
    }
    
      public List<Object> getLstEvenement(int id){

        List<Object> lst = getRequest(id);        
        for(Object obj : lst){
            ConsoleOutput.addOutPut(obj.toString());
        }
        
        return lst;
    }
    
    public List<Object> getRequest(int id)
    {
        ClientConfig config = new DefaultClientConfig();
        Client client = Client.create(config);
        WebResource service = client.resource(UriBuilder.fromUri("http://127.0.0.1:9001").build());
         
        MultivaluedMap queryParams = new MultivaluedMapImpl();
        queryParams.add("id", String.valueOf(id));
        
        String json;
        json = service.path("bdeproject").path("evenement").queryParams(queryParams).accept(MediaType.APPLICATION_JSON).get(String.class);
   
        Genson genson = new Genson.Builder().setWithClassMetadata(true).create();
        
        List<Object> lst = null;

        try{
          lst = genson.deserialize(json, List.class);
        }
        catch(Exception ex){
            System.out.println(ex.getMessage());
        }
  
        return lst;
    }
      
    
    public List<Object> getRequest()
    {
        ClientConfig config = new DefaultClientConfig();
        Client client = Client.create(config);
        WebResource service = client.resource(UriBuilder.fromUri("http://127.0.0.1:9001").build());
        
        String json;
        json = service.path("bdeproject").path("evenement").accept(MediaType.APPLICATION_JSON).get(String.class);
        
        // Genson genson = new Genson();
        Genson genson = new Genson.Builder().setWithClassMetadata(true).create();
        
        List<Object> lst = null;
        List<Evenement>  listEv = null;
       // List<Eleve> lst = null;
        
        try{
          lst = genson.deserialize(json, List.class);
          listEv = genson.deserialize(json, List.class);
          
            //lst = genson.deserialize(json, new GenericType<List<Eleve>>(){});
        }
        catch(Exception ex){
            System.out.println(ex.getMessage());
        }
      
        //List<Eleve> listEleve = (List<Object>)(List<?>)lst;
        
        
        GridView view = new GridView();
        view.setVisible(true);
        
        javax.swing.JTable jTable = view.getjTable1();
        
        DefaultTableModel model = new DefaultTableModel(); 
jTable.setModel(model);
// Create a couple of columns 
model.addColumn("IDEvenement"); 
model.addColumn("Libelle"); 
model.addColumn("Type"); 
model.addColumn("Duree"); 
model.addColumn("Date"); 

// Append a row 


for(Evenement ev : listEv){
          //  System.out.println(ev);
         //   System.out.println(ev.getIdEvenement());
            model.addRow(new Object[]{ev.getIdEvenement(), ev.getLibelle(),ev.getType(),ev.getDuree(),ev.getDate()});
        }
     //   System.out.println(lst.size());
        
        return lst;
    }
    
     public List<Evenement> postRequest()
    {
        
        ClientConfig config = new DefaultClientConfig();
        Client client = Client.create(config);
        WebResource service = client.resource(UriBuilder.fromUri("http://127.0.0.1:9001").build());
        
        String json;

        Evenement event = new Evenement("Alexis",65, "Toto","");
       
        Genson genson = new Genson.Builder().setWithClassMetadata(true).create();

        try {
            json = genson.serialize(event);
        }
        catch (Exception ex) {
            json = "{" + ex.getMessage() + "}";
        }
       
        
	ClientResponse response = service.path("bdeproject").path("evenement").type(MediaType.APPLICATION_FORM_URLENCODED_TYPE).post(ClientResponse.class, json);
	
        System.out.println("Response " + response.getEntity(String.class));
        
  /*     
        MultivaluedMap formData = new MultivaluedMapImpl();
        formData.add("name1", "val1");
        formData.add("name2", "val2");
        ClientResponse response = webResource.type(MediaType.APPLICATION_FORM_URLENCODED_TYPE).post(ClientResponse.class, formData);
        System.out.println("Response " + response.getEntity(String.class));
               
        String json;
        
        json = service.path("bdeproject").path("eleve").accept(MediaType.APPLICATION_JSON).get(String.class);
        
       // json = service.path("bdeproject").path("evenement").accept(MediaType.APPLICATION_JSON).get(String.class);
        
       // json = service.path("bdeproject").path("groupe").accept(MediaType.APPLICATION_JSON).get(String.class);

        // getting XML data
        System.out.println(json);
        // getting JSON data
       // System.out.println(service.path("restPath").path("resourcePath").accept(MediaType.APPLICATION_XML).get(String.class));
        
        // Genson genson = new Genson();
        Genson genson = new Genson.Builder().setWithClassMetadata(true).create();
        
        List<Object> lst = null;
        
        try{
            lst = genson.deserialize(json, List.class);
        }
        catch(Exception ex){
            System.out.println(ex.getMessage());
        }
        
        for(Object obj : lst){
             System.out.println(obj.toString());
        }
       
        //List<Eleve> listEleve = (List<Object>)(List<?>)lst;
        
        System.out.println(lst.size());
        */
        return null;
    }
     
      public List<Evenement> postRequest(Evenement event_)
    {
        
        ClientConfig config = new DefaultClientConfig();
        Client client = Client.create(config);
        WebResource service = client.resource(UriBuilder.fromUri("http://127.0.0.1:9001").build());
        
        String json;

        Evenement event = event_;
       
        Genson genson = new Genson.Builder().setWithClassMetadata(true).create();

        try {
            json = genson.serialize(event);
        }
        catch (Exception ex) {
            json = "{" + ex.getMessage() + "}";
        }
       
        
	ClientResponse response = service.path("bdeproject").path("evenement").type(MediaType.APPLICATION_FORM_URLENCODED_TYPE).post(ClientResponse.class, json);
	
        System.out.println("Response " + response.getEntity(String.class));
        
  /*     
        MultivaluedMap formData = new MultivaluedMapImpl();
        formData.add("name1", "val1");
        formData.add("name2", "val2");
        ClientResponse response = webResource.type(MediaType.APPLICATION_FORM_URLENCODED_TYPE).post(ClientResponse.class, formData);
        System.out.println("Response " + response.getEntity(String.class));
               
        String json;
        
        json = service.path("bdeproject").path("eleve").accept(MediaType.APPLICATION_JSON).get(String.class);
        
       // json = service.path("bdeproject").path("evenement").accept(MediaType.APPLICATION_JSON).get(String.class);
        
       // json = service.path("bdeproject").path("groupe").accept(MediaType.APPLICATION_JSON).get(String.class);

        // getting XML data
        System.out.println(json);
        // getting JSON data
       // System.out.println(service.path("restPath").path("resourcePath").accept(MediaType.APPLICATION_XML).get(String.class));
        
        // Genson genson = new Genson();
        Genson genson = new Genson.Builder().setWithClassMetadata(true).create();
        
        List<Object> lst = null;
        
        try{
            lst = genson.deserialize(json, List.class);
        }
        catch(Exception ex){
            System.out.println(ex.getMessage());
        }
        
        for(Object obj : lst){
             System.out.println(obj.toString());
        }
       
        //List<Eleve> listEleve = (List<Object>)(List<?>)lst;
        
        System.out.println(lst.size());
        */
        return null;
    }
}
