/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.main.bdeproject.service;

import com.main.bdeproject.bean.Eleve;
import com.main.bdeproject.view.ConsoleOutput;
import com.owlike.genson.Genson;
import java.util.List;
import java.io.IOException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;
import org.apache.http.client.ClientProtocolException;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.GenericType;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;

/**
 *
 * @author Windows-Work
 */
public class ServiceEleve {
    
    public List<Object> getLstEleve(){

        List<Object> lst = getRequest();        
        for(Object obj : lst){
            ConsoleOutput.addOutPut(obj.toString());
        }
        
        return lst;
    }
    
    public List<Object> getRequest()
    {
        ClientConfig config = new DefaultClientConfig();
        Client client = Client.create(config);
        WebResource service = client.resource(UriBuilder.fromUri("http://127.0.0.1:9001").build());
        
        String json;
        
        json = service.path("bdeproject").path("eleve").accept(MediaType.APPLICATION_JSON).get(String.class);
        
       // json = service.path("bdeproject").path("evenement").accept(MediaType.APPLICATION_JSON).get(String.class);
       // json = service.path("bdeproject").path("groupe").accept(MediaType.APPLICATION_JSON).get(String.class);

        // getting XML data
        System.out.println(json);
        // getting JSON data
       // System.out.println(service.path("restPath").path("resourcePath").accept(MediaType.APPLICATION_XML).get(String.class));
        
        // Genson genson = new Genson();
        Genson genson = new Genson.Builder().setWithClassMetadata(true).create();
        
        List<Object> lst = null;
        
       // List<Eleve> lst = null;
        
        try{
          lst = genson.deserialize(json, List.class);
            //lst = genson.deserialize(json, new GenericType<List<Eleve>>(){});
        }
        catch(Exception ex){
            System.out.println(ex.getMessage());
        }
      
        //List<Eleve> listEleve = (List<Object>)(List<?>)lst;
        
     //   System.out.println(lst.size());
        
        return lst;
    }
    
     public List<Object> postRequest()
    {
        
        ClientConfig config = new DefaultClientConfig();
        Client client = Client.create(config);
        WebResource service = client.resource(UriBuilder.fromUri("http://127.0.0.1:9001").build());
        
        String json;

        Eleve eleve = new Eleve("Alexis", "Landrieu", "Toto");
        
     //   json = service.path("bdeproject").path("eleve").accept(MediaType.APPLICATION_JSON).post(eleve);
        
        
  /*     
        MultivaluedMap formData = new MultivaluedMapImpl();
        formData.add("name1", "val1");
        formData.add("name2", "val2");
        ClientResponse response = webResource.type(MediaType.APPLICATION_FORM_URLENCODED_TYPE).post(ClientResponse.class, formData);
        System.out.println("Response " + response.getEntity(String.class));
               
        String json;
        
        json = service.path("bdeproject").path("eleve").accept(MediaType.APPLICATION_JSON).get(String.class);
        
       // json = service.path("bdeproject").path("evenement").accept(MediaType.APPLICATION_JSON).get(String.class);
        
       // json = service.path("bdeproject").path("groupe").accept(MediaType.APPLICATION_JSON).get(String.class);

        // getting XML data
        System.out.println(json);
        // getting JSON data
       // System.out.println(service.path("restPath").path("resourcePath").accept(MediaType.APPLICATION_XML).get(String.class));
        
        // Genson genson = new Genson();
        Genson genson = new Genson.Builder().setWithClassMetadata(true).create();
        
        List<Object> lst = null;
        
        try{
            lst = genson.deserialize(json, List.class);
        }
        catch(Exception ex){
            System.out.println(ex.getMessage());
        }
        
        for(Object obj : lst){
             System.out.println(obj.toString());
        }
       
        //List<Eleve> listEleve = (List<Object>)(List<?>)lst;
        
        System.out.println(lst.size());
        */
        return null;
    }
}
