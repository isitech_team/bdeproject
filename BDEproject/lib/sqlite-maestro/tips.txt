A column that is declared as INTEGER PRIMARY KEY would be autoincrement.
SQLite does not support the length of a VARCHAR. You can declare a VARCHAR(10) and SQLite will be happy to let you put 500 characters in it. And it will keep all 500 characters intact - it never truncates.
SQLite versions 3.0 and later allows you to put BLOB data into any column, even if the column is declared to hold some other type.
SQLite has limited ALTER TABLE support that you can use this command to add a column to the end of a table or to change the name of a table. If you what to make more complex changes to the structure of a table, you need to recreate the table.
When you delete information from an SQLite database, the unused disk space is added to an internal "free-list" and is reused the next time you insert data. The disk space is not lost. But neither is it returned to the operating system.
Version 2 of SQLite stores all column values as ASCII text. Version 3 enhances this by providing the ability to store integer and real numbers in a more compact format and the capability to store BLOB data.
Running VACUUM after the ALTER TABLE restores the database to the format that it can be read by earlier SQLite versions.
The version 3 file format is very different and is completely incompatible with the version 2 file format. Version 3.0.0 is a major upgrade for SQLite that incorporates support for UTF-16, BLOBs, and a more compact encoding that results in database files that are typically 25% to 50% smaller.
Information about views is stored in the SQLITE_MASTER table.
FOREIGN KEY constraints are parsed but are not enforced by SQLite engine.
CHECK constraints are parsed but they are not enforced by SQLIte engine.
The current implementation of SQLite allows only a single active transaction.
Since SQLite reads and writes an ordinary disk file, the only access permissions that can be applied are the normal file access permissions of the underlying operating system.