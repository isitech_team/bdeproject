BDEproject
===========

Copyright (C) 2015, BDEproject (ISITECH Student team).

------------------------------------------------------------------------

Project Status
------------
**Close 13/02/2015**


Installation
------------
no install requirement

Quick Start
-----------
* Server 
```sh
run.bat 
```

* Service Consume GET Sample 
```sh
   GET http://localhost:9001/bdeproject/evenement
   GET http://localhost:9001/bdeproject/evenement?id=5
```

* Service Consume POST Sample 
```sh
   POST http://localhost:9001/bdeproject/evenement
	HTTP BODY [{"@class":"com.main.bdeproject.bean.Evenement","date":"2014-11-24 12:00:37","duree":23,"libelle":"SLDSKI - Sorties de ski au départ de lyon. Journées et week-end de ski en Autocar.","type":"SKI"}]
   POST http://localhost:9001/bdeproject/groupe
    HTTP BODY [{"@class":"com.main.bdeproject.bean.Groupe","listEleve":null,"nomGroupe":"ISITECH"}]
```

* Java Client
```sh
runClient.bat
```

**Warning before run scripts check JAVA_HOME variable, programs need JRE8.**

Documentation
-----------

* ./BDEproject/doc

Contributors
-------------
* Alexis LANDRIEU, Manager mise en place de la structure et implémentation du serveur de webservice.
* Alexis QUESNEL, Réalisation de la partie DAO de l'application et une partie de l'application de démonstration.
* Arnaud TEPPAZ, Aide à la réalisation des beans et des services.
* Ali BELHOCINE, Réalisation des beans et des services.
* Quentin MIGLIORE, Réalisation des beans et des services.
* Guillaume APPY, Population, création, branchement sur le serveur et administration de la base de données.

License
------------
It is released under the GNU General Public License (GPL).
 
See the LICENSE file for license rights and limitations (GNU GPL).