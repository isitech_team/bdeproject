echo "Start Program Java hackit_clientJ"
set JAVA_HOME="C:\Program Files\Java\jre1.8.0_31\bin\java"
set CURRENT_PATH=%CD%

cd BDEproject
%JAVA_HOME% -classpath "%CURRENT_PATH%\BDEproject\bin;%CURRENT_PATH%\BDEproject\lib\restlet-jse-2.3.1\lib\org.restlet.jar;%CURRENT_PATH%\BDEproject\lib\gson-2.3.1.jar;%CURRENT_PATH%\BDEproject\lib\junit-4.12.jar;%CURRENT_PATH%\BDEproject\lib\hamcrest-core-1.3.jar;%CURRENT_PATH%\BDEproject\lib\sqlite-jdbc-3.8.7.jar;%CURRENT_PATH%\BDEproject\lib\genson-0.98.jar" com.main.bdeproject.Principale

cd ..